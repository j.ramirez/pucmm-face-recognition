Atributos de calidad:

1. Rendimiento.
2. Seguridad.
3. Usabilidad


Diagrama:
![Diagram](diagram.jpg)


Capturas:
![Panel](panel.png)
![Stream](stream.png)



Frameworks:
* OpenCV
* face-recognition.js
* ExpressJS
* NodeJs
* ReactJs
* Semantic UI
* face-detection-node-opencv
* face-api.js
* Kairos (AS EXTERNAL SERVICE)


